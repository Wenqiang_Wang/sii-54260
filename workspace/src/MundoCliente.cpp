//Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <unistd.h>
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>

#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <time.h>
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
#define NCH 200
MundoCliente::MundoCliente()
{
	Init();
}

MundoCliente::~MundoCliente()
{
//	close(fd_sc);
//	close(fd_cs);
/*
	munmap(dato1,sizeof(dato1));
	munmap(dato2,sizeof(dato2));
	unlink("dato1.txt");
	unlink("dato2.txt");
*/
}

void MundoCliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);

	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++)
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );

	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);

	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}

void MundoCliente::OnDraw()
{
	//Borrado de la pantalla
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0)
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	for(int i=0;i<esferas.size();i++)
		esferas[i].Dibuja();
//	for(int i=0;i<disparos1.size();i++)
//		disparos1[i].Dibuja();
//	for(int i=0;i<disparos2.size();i++)
//		disparos2[i].Dibuja();
	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void MundoCliente::OnTimer(int value)
{
/*
	float speed=0.025f;
	if(temporizador_inmoviliza1<=0)jugador1.Mueve(speed);
	if(temporizador_inmoviliza2<=0)jugador2.Mueve(speed);
	for(int i=0;i<esferas.size();i++)
		esferas[i].Mueve(speed);
	for(int i=0;i<disparos1.size();i++)
		disparos1[i].Mueve(speed);
	for(int i=0;i<disparos2.size();i++)
		disparos2[i].Mueve(speed);


	int i;
	for(i=0;i<paredes.size();i++)
	{
		for(int j=0;j<esferas.size();j++)
			paredes[i].Rebota(esferas[j]);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}
*/

//disparos
/*
	for(int i=0;i<disparos1.size();i++)
	{
		if(jugador2.Rebota(disparos1[i]))
		{
			disparos1.erase(disparos1.begin()+i);
//			disparos1.shrink_to_fit();
			if(temporizador_inmoviliza2==0)temporizador_inmoviliza2=100;
		}
		else if(fondo_dcho.Rebota(disparos1[i]))
		{
                        disparos1.erase(disparos1.begin()+1);
//			disparos1.shrink_to_fit();
                }

	}
	for(int i=0;i<disparos2.size();i++)
                {
                if(jugador1.Rebota(disparos2[i])){
                        disparos2.erase(disparos2.begin()+i);
//                        disparos2.shrink_to_fit();
                        if(temporizador_inmoviliza1==0)temporizador_inmoviliza1=100;
		}
		else if(fondo_izq.Rebota(disparos2[i]))
                {
                        disparos2.erase(disparos2.begin()+1);
//                        disparos2.shrink_to_fit();
                }

        }
*/

/*
	for(int i=0;i<esferas.size();i++){
		jugador1.Rebota(esferas[i]);
		jugador2.Rebota(esferas[i]);
		if(fondo_izq.Rebota(esferas[i]))
		{
//			esferas[i].centro.x=0;
//			esferas[i].centro.y=rand()/(float)RAND_MAX;
//			esferas[i].velocidad.x=2+2*rand()/(float)RAND_MAX;
//			esferas[i].velocidad.y=2+2*rand()/(float)RAND_MAX;
//			esferas[i].radio=0.5f;
			esferas.erase(esferas.begin()+i);
//			esferas.shrink_to_fit();
			puntos2++;
//			char cad[200];
//			sprintf(cad,"Juagador 2 marca 1 punto ,lleva un total de %d puntos. \n",puntos2);
//			write(fd,&cad,strlen(cad)+1);

//			if(puntos2==10){
//			sprintf(cad,"Jugador 2 haganado. \nLa pratida ha ternimado.\n");
//			write(fd,&cad,strlen(cad)+1);
//			glutDestroyWindow(glutGetWindow());
			};
		}

		if(fondo_dcho.Rebota(esferas[i]))
		{
//			esferas[i].centro.x=0;
//			esferas[i].centro.y=rand()/(float)RAND_MAX;
//			esferas[i].velocidad.x=-2-2*rand()/(float)RAND_MAX;
//			esferas[i].velocidad.y=-2-2*rand()/(float)RAND_MAX;
//			esferas[i].radio=0.5f;
			esferas.erase(esferas.begin()+i);
//			esferas.shrink_to_fit();
			puntos1++;
//			char cad[200];
//	        	sprintf(cad,"Juagador 1 marca 1 punto ,lleva un total de %d puntos. \n",puntos1);
//        		write(fd,&cad,strlen(cad)+1);

//			if(puntos1==10){
//			sprintf(cad,"Jugador 1 haganado. \nLa pratida ha ternimado.\n");
//			write(fd,&cad,strlen(cad)+1);
//			glutDestroyWindow(glutGetWindow());
			};

		}
	}
*/
/*
//esferas
	temporizador_bola++;
	if(temporizador_bola>200)
	{
	temporizador_bola=0;
	Esfera e;
	e.velocidad.x=(rand()%2)?3:-3;
	e.velocidad.y=(rand()%2)?3:-3;
	esferas.push_back(e);
	}
//disparos
	temporizador_disparo1++;
	temporizador_disparo2++;
	if(temporizador_inmoviliza1>0)temporizador_inmoviliza1--;
	if(temporizador_inmoviliza2>0)temporizador_inmoviliza2--;
*/


//lectura de datos desde el cliente
	char cad[NCH];
	int n_esferas;
//	read(fd_sc,&cad,NCH);
	s_comunicacion.Receive(cad,NCH);
	sscanf(cad,"%d",&n_esferas);
	while(esferas.size()!=n_esferas){
		if(esferas.size()<n_esferas){
			Esfera e;
			esferas.push_back(e);
		}
		else if(esferas.size()>n_esferas)
			esferas.pop_back();
	};

	for(int i=0;i<n_esferas;i++){
//		read(fd_sc,&cad,NCH);
		s_comunicacion.Receive(cad,NCH);
		sscanf(cad,"%f %f",&esferas[i].centro.x,&esferas[i].centro.y);
	};
//	read(fd_sc,&cad,NCH);
	s_comunicacion.Receive(cad,NCH);
	sscanf(cad,"%f %f %f %f %d",&jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2,&puntos1);
//	read(fd_sc,&cad,NCH);
	s_comunicacion.Receive(cad,NCH);
	sscanf(cad,"%f %f %f %f %d",&jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2,&puntos2);








//dato1
	int id=0;
	for(int i=0;i<esferas.size();i++)
		if(esferas[i].centro.x<esferas[id].centro.x)
			id=i;
	dato1->esfera=esferas[id];
	dato1->raqueta=jugador1;
	if(dato1->accion!=0){
		char k=(dato1->accion<1)?'s':'w';
                char cad[NCH];
                sprintf(cad,"%c",k);
//		write(fd_cs,&cad,NCH);
		s_comunicacion.Send(cad,NCH);

	};
//dato1
        for(int i=0;i<esferas.size();i++)
                if(esferas[i].centro.x>esferas[id].centro.x)
                        id=i;
        dato2->esfera=esferas[id];
        dato2->raqueta=jugador2;

//temporizador ausencia
	temporizador_ausencia++;
	if(temporizador_ausencia>400){
		if(dato2->accion!=0){
			char k=(dato2->accion<1)?'l':'o';
			char cad[NCH];
	        	sprintf(cad,"%c",k);
//	        	write(fd_cs,&cad,NCH);
			s_comunicacion.Send(cad,NCH);

		};
//		jugador2.velocidad.y=dato2->accion*4;
	};
	if(temporizador_ausencia>32000)temporizador_ausencia=401;



}

void MundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
/*
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
//	case 's':jugador1.velocidad.y=-4;break;
//	case 'w':jugador1.velocidad.y=4;break;
//	case 'l':jugador2.velocidad.y=-4;break;
//	case 'o':jugador2.velocidad.y=4;break;

//disparos

	case 'd':
		if(temporizador_disparo1>50)
		{
			temporizador_disparo1=0;
			Esfera e;e.radio=0.2f;
			e.velocidad.x=5;e.velocidad.y=0;
			e.centro.x=jugador1.x1;
			e.centro.y=(jugador1.y1+jugador1.y2)/2;
			e.g=0;
			disparos1.push_back(e);
		}
		break;
	case 'k':
		if(temporizador_disparo2>50)
		{
			temporizador_disparo2=0;
			Esfera e;e.radio=0.2f;
                	e.velocidad.x=-5;e.velocidad.y=0;
                	e.centro.x=jugador2.x1;
                	e.centro.y=(jugador2.y1+jugador2.y2)/2;
                	e.g=0;
                	disparos2.push_back(e);
		}
		break;

	}
*/
	char cad[NCH];
	sprintf(cad,"%c",key);
//	write(fd_cs,&cad,NCH);
	s_comunicacion.Send(cad,NCH);
	temporizador_ausencia=0;
}

void MundoCliente::Init()
{
	temporizador_bola=200;
//	temporizador_disparo1=100;
//	temporizador_disparo2=100;
//	temporizador_inmoviliza1=0;
//	temporizador_inmoviliza2=0;
	temporizador_ausencia=0;
	srand(time(NULL));
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
//esfera
//	Esfera e;
//	esferas.push_back(e);

//logger
//	fd=open("log.txt",O_WRONLY);
//	if(fd==-1){
//	perror("open");
//	return;
//	};

/*
//fifo sc
	char path[]="sc.txt";
	if(mkfifo(path,0666)==0){
		perror("mkfifo cliente");
		return;
	};
	fd_sc=open(path,O_RDONLY);
	if(fd_sc==-1){
		perror("open cliente");
		return;
	};

//fifo cs

	strcpy(path,"cs.txt");
	if(mkfifo(path,0666)==0){
		perror("mkfifo cliente2");
		return;
	};
	fd_cs=open(path,O_WRONLY);
	if(fd_cs==-1){
		perror("open cliente 2");
		return;
	};

*/
//conexion servidor

	char ip[]="127.0.0.1";
	char nombre[20];
	printf("Introduzca tu nombre. \n");
	scanf("%s",nombre);
	s_comunicacion.Connect(ip,8000);
	s_comunicacion.Send(nombre,sizeof(nombre));

//dato1
	int fddato;
	fddato=open("dato1.txt",O_CREAT|O_TRUNC|O_RDWR,0640);
	write(fddato,"",sizeof(DatosMemCompartida));
	dato1=static_cast<DatosMemCompartida*>(mmap(NULL,sizeof(DatosMemCompartida),PROT_READ|PROT_WRITE,MAP_SHARED,fddato,0));
	close(fddato);
//dato2
	fddato=open("dato2.txt",O_CREAT|O_TRUNC|O_RDWR,0640);
        write(fddato,"",sizeof(DatosMemCompartida));
        dato2=static_cast<DatosMemCompartida*>(mmap(NULL,sizeof(DatosMemCompartida),PROT_READ|PROT_WRITE,MAP_SHARED,fddato,0));
        close(fddato);

}
