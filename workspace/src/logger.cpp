#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>



int main(int argc,char* argv[])
{
	int fd;
	char ch;
	char path[]="log.txt";

	if(mkfifo(path,0666)==0){
		return 1;
		perror(argv[0]);
	};
	fd=open(path,O_RDONLY);
	if(fd==-1){
		perror(argv[0]);
		return 1;
	};
	while(read(fd,&ch,1)==1){
		write(1,&ch,1);
	};

	close(fd);

	return 0;
};
