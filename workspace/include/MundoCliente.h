// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"
#include "Socket.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DatosMemCompartida.h"
class MundoCliente
{
public:
	void Init();
	MundoCliente();
	virtual ~MundoCliente();

	void InitGL();
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();

	std::vector<Esfera> esferas;
//	std::vector<Esfera> disparos1;
//	std::vector<Esfera> disparos2;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	int temporizador_bola;
//	int temporizador_disparo1,temporizador_disparo2;
//	int temporizador_inmoviliza1,temporizador_inmoviliza2;
	int temporizador_ausencia;
	int puntos1;
	int puntos2;
//	int fd;
	DatosMemCompartida* dato1;
	DatosMemCompartida* dato2;
//	int fd_sc;
//	int fd_cs;
	Socket s_comunicacion;

};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)

