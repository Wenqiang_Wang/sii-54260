
#include <vector>
#include <Raqueta.h>
#include <pthread.h>
#include "Plano.h"
#include "Socket.h"
#pragma once

//#include "DatosMemCompartida.h"
class MundoServidor
{
public:
	void Init();
	MundoServidor();
	virtual ~MundoServidor();

	void InitGL();
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();
	void RecibeComandosJugador();

	std::vector<Esfera> esferas;
//	std::vector<Esfera> disparos1;
//	std::vector<Esfera> disparos2;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	int temporizador_bola;
//	int temporizador_disparo1,temporizador_disparo2;
//	int temporizador_inmoviliza1,temporizador_inmoviliza2;
	int temporizador_ausencia;
	int puntos1;
	int puntos2;
	int fd;
//	int fd_sc;
//	int fd_cs;
	pthread_attr_t atrib;
	pthread_t thid1;
//	DatosMemCompartida* dato1;
//	DatosMemCompartida* dato2;
	Socket s_conexion;
	Socket s_comunicacion;
};


